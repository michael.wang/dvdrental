To practice golang by using DVD rental as user story.

* DB from PostgreSQL Tutorial: https://www.postgresqltutorial.com/postgresql-sample-database/
* PostgreSQL client and ORM for Golang: https://github.com/go-pg/pg
* gRPC for Golang: https://grpc.io/docs/tutorials/basic/go/
* Micro framework: https://micro.mu/docs/index.html

I use progressive/agile development process, meaning I don't add feature until it is needed. For example, I don't apply golang project layout upfront (https://github.com/golang-standards/project-layout). As project getting compicated, at some point I am gonna added it.

I do apply gitflow upfront: https://ihower.tw/blog/archives/5140, so contributors please follow gitflow too. (Working on feature branch, merge back to develop branch when done review and accepted.)

- Michael Wang (michael.icwang@gmail.com)
